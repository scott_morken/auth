<?php

declare(strict_types=1);

namespace Smorken\Auth\Factories;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use Smorken\Auth\Contracts\Models\User;
use Smorken\Auth\Contracts\Repositories\FilteredUsersRepository;
use Smorken\Auth\Contracts\Repositories\FindUserRepository;
use Smorken\Domain\Factories\RepositoryFactory;
use Smorken\QueryStringFilter\Contracts\QueryStringFilter;
use Smorken\Support\Contracts\Filter;

class UserRepositoryFactory extends RepositoryFactory
{
    protected array $handlers = [
        'find' => FindUserRepository::class,
        'filtered' => FilteredUsersRepository::class,
    ];

    public function emptyModel(): mixed
    {
        return $this->handlerForEmptyModel();
    }

    public function filtered(
        QueryStringFilter|Filter $filter,
        int $perPage = 20
    ): Paginator|Collection|iterable {
        return $this->handlerForFiltered($filter, $perPage);
    }

    public function find(mixed $id, bool $throw = true): ?User
    {
        return $this->handlerForFind($id, $throw);
    }
}
