<?php

declare(strict_types=1);

namespace Smorken\Auth\Models\VO;

use Illuminate\Foundation\Auth\Access\Authorizable;
use Smorken\Auth\Models\Concerns\HasName;
use Smorken\Auth\Models\Concerns\IsAuthenticatable;
use Smorken\Auth\Models\Concerns\IsStringable;
use Smorken\Model\VO;

class User extends VO implements \Smorken\Auth\Contracts\Models\User
{
    use Authorizable, HasName, IsAuthenticatable, IsStringable;

    public function getAuthPasswordName(): string
    {
        return 'password';
    }

    public function isValid(): bool
    {
        return $this->id && ! str_contains((string) $this->id, 'ERROR');
    }
}
