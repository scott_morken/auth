<?php

declare(strict_types=1);

namespace Smorken\Auth\Models\Concerns;

trait IsAuthenticatable
{
    public function getAuthIdentifier(): string|int
    {
        return $this->getAttribute($this->getAuthIdentifierName());
    }

    public function getAuthIdentifierName(): string
    {
        return 'id';
    }

    public function getAuthPassword(): string
    {
        return '*SOCIAL*';
    }

    public function getLogin(): string
    {
        return $this->getAttribute($this->getLoginField());
    }

    public function getLoginField(): string
    {
        return 'username';
    }

    public function getRememberToken(): ?string
    {
        return null;
    }

    public function getRememberTokenName(): string
    {
        return 'remember_me';
    }

    public function setRememberToken($value): void {}
}
