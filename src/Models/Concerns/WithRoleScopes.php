<?php

namespace Smorken\Auth\Models\Concerns;

use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Smorken\Roles\Models\Eloquent\Role;
use Smorken\Roles\Models\Eloquent\RoleUser;

trait WithRoleScopes
{
    public function doesntHaveRole(): EloquentBuilder
    {
        return $this->doesntHave('role');
    }

    public function hasRole(): EloquentBuilder
    {
        return $this->has('role');
    }

    public function hasRoleCode(string $roleCode): EloquentBuilder
    {
        return $this->whereHas('role', function (EloquentBuilder $q) use ($roleCode) {
            $q->where('code', '=', $roleCode);
        });
    }

    public function roleIncludes(int $role): EloquentBuilder
    {
        return $this->whereHas('role', function ($q) use ($role) {
            return $q->where('level', '>=', $role);
        });
    }

    public function roleIncludesCode(string $code): EloquentBuilder
    {
        // @phpstan-ignore return.type
        return $this->whereExists($this->includesCodeSubquery($code));
    }

    public function roleIs(int $role): EloquentBuilder
    {
        $query = $this->whereHas('role', function ($q) use ($role) {
            return $q->where('level', '=', $role);
        });
        if ((int) $role === 0) {
            $query = $query->orWhereDoesntHave('role');
        }

        return $query;
    }

    protected function includesCodeSubquery(string $code): EloquentBuilder
    {
        $userTable = $this->getModel()->getTable();
        $roleUserTable = (new RoleUser)->getTable();
        $roleTable = (new Role)->getTable();

        // @phpstan-ignore return.type
        return (new RoleUser)->newQuery()
            ->join($roleTable, $roleTable.'.id', '=', $roleUserTable.'.role_id')
            ->where($roleTable.'.level', '>=',
                function (\Illuminate\Contracts\Database\Query\Builder $q) use ($code, $roleTable) {
                    $q->select('level')
                        ->from($roleTable)
                        ->where('code', '=', $code);
                })
            ->whereColumn($userTable.'.id', '=', $roleUserTable.'.user_id');
    }

    protected function joinCodeIsSubquery(EloquentBuilder $query, string $code): EloquentBuilder
    {
        // @phpstan-ignore return.type
        return $query->select('level')->where('code', '=', $code)->limit(1);
    }
}
