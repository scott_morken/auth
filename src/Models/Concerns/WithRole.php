<?php

namespace Smorken\Auth\Models\Concerns;

use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasOneThrough;
use Smorken\Roles\Contracts\Models\Role;
use Smorken\Roles\Models\Eloquent\RoleUser;

trait WithRole
{
    public function hasRole(Role $role): bool
    {
        return ($this->role?->level ?? 0) >= $role->level;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOneThrough<\Smorken\Roles\Models\Eloquent\Role, \Smorken\Roles\Models\Eloquent\RoleUser, $this>
     */
    public function role(): HasOneThrough
    {
        return $this->hasOneThrough(
            \Smorken\Roles\Models\Eloquent\Role::class,
            RoleUser::class,
            'user_id',
            'id',
            'id',
            'role_id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne<\Smorken\Roles\Models\Eloquent\RoleUser, $this>
     */
    public function roleUser(): HasOne
    {
        return $this->hasOne(RoleUser::class);
    }
}
