<?php

declare(strict_types=1);

namespace Smorken\Auth\Models\Concerns;

trait IsStringable
{
    public function __toString(): string
    {
        return $this->name();
    }
}
