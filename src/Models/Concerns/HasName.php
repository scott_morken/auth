<?php

declare(strict_types=1);

namespace Smorken\Auth\Models\Concerns;

use Illuminate\Database\Eloquent\Casts\Attribute;

trait HasName
{
    public function fullName(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->name()
        );
    }

    public function name(): string
    {
        return implode(' ', array_filter([$this->first_name, $this->last_name]));
    }

    public function shortName(): Attribute
    {
        return Attribute::make(
            get: fn () => implode(' ', array_filter([$this->getFirstInitial(), $this->last_name]))
        );
    }

    protected function getFirstInitial(): ?string
    {
        $first = substr($this->first_name, 0, 1);
        if (strlen($first)) {
            return $first.'.';
        }

        return null;
    }
}
