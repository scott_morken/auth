<?php

declare(strict_types=1);

namespace Smorken\Auth\Models\Eloquent;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\HasBuilder;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Smorken\Auth\Contracts\Concerns\CanPersist;
use Smorken\Auth\Models\Builders\UserBuilder;
use Smorken\Auth\Models\Concerns\HasName;
use Smorken\Auth\Models\Concerns\IsAuthenticatable;
use Smorken\Auth\Models\Concerns\IsStringable;
use Smorken\Auth\Models\Concerns\WithRole;
use Smorken\Model\Eloquent;

class User extends Eloquent implements \Smorken\Auth\Contracts\Models\User, CanPersist
{
    use Authorizable, HasFactory, HasName, IsAuthenticatable, IsStringable, WithRole;

    /** @use HasBuilder<UserBuilder<static>> */
    use HasBuilder;

    /** @use HasFactory<\Database\Factories\Smorken\Auth\Models\Eloquent\UserFactory> */
    use HasFactory;

    public $incrementing = false;

    protected ?string $activeRole = null;

    protected $attributes = [
        'password' => '*SOCIAL*',
    ];

    protected static string $builder = UserBuilder::class;

    protected $fillable = ['id', 'username', 'first_name', 'last_name', 'email'];

    protected $hidden = ['password'];

    protected $keyType = 'string';

    public function activeRole(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->getActiveRole(),
        );
    }

    public function getAuthPasswordName(): string
    {
        return 'password';
    }

    public function isValid(): bool
    {
        return $this->id && ! str_contains((string) $this->id, 'ERROR');
    }

    public function username(): Attribute
    {
        return Attribute::make(
            set: fn (string $value) => $this->attributes['username'] = strtolower($value),
        );
    }

    protected function getActiveRole(): string
    {
        if ($this->isValid() && $this->activeRole === null) {
            $this->activeRole = $this->role?->descr ?? '';
        }

        return $this->activeRole;
    }
}
