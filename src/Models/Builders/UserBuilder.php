<?php

declare(strict_types=1);

namespace Smorken\Auth\Models\Builders;

use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Smorken\Auth\Models\Concerns\WithRoleScopes;
use Smorken\Model\Filters\FilterHandler;
use Smorken\Model\QueryBuilders\Builder;
use Smorken\QueryStringFilter\Concerns\WithQueryStringFilter;

/**
 * @template TModel of \Smorken\Auth\Models\Eloquent\User
 *
 * @extends Builder<TModel>
 */
class UserBuilder extends Builder
{
    use WithQueryStringFilter, WithRoleScopes;

    public function defaultOrder(): EloquentBuilder
    {
        // @phpstan-ignore return.type
        return $this->orderBy('last_name')
            ->orderBy('first_name');
    }

    public function firstNameLike(string $first): EloquentBuilder
    {
        return $this->where('first_name', 'LIKE', $first.'%');
    }

    public function idIs(int|string $id): EloquentBuilder
    {
        return $this->where('id', '=', $id);
    }

    public function lastNameLike(string $last): EloquentBuilder
    {
        return $this->where('last_name', 'LIKE', $last.'%');
    }

    public function usernameIs(string $username): EloquentBuilder
    {
        return $this->where('username', '=', $username);
    }

    protected function getFilterHandlersForFilters(): array
    {
        return [
            new FilterHandler('firstName', 'firstNameLike'),
            new FilterHandler('lastName', 'lastNameLike'),
            new FilterHandler('userId', 'idIs'),
            new FilterHandler('username', 'usernameIs'),
            new FilterHandler('role', 'roleIs'),
        ];
    }
}
