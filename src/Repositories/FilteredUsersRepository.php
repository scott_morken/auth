<?php

declare(strict_types=1);

namespace Smorken\Auth\Repositories;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Smorken\Auth\Contracts\Models\User;
use Smorken\Domain\Repositories\EloquentFilteredRepository;

/**
 * @extends EloquentFilteredRepository<\Smorken\Auth\Models\Eloquent\User>
 */
class FilteredUsersRepository extends EloquentFilteredRepository implements \Smorken\Auth\Contracts\Repositories\FilteredUsersRepository
{
    public function __construct(User $model)
    {
        parent::__construct($model);
    }

    /**
     * @param  \Smorken\Auth\Models\Builders\UserBuilder  $query
     * @return \Smorken\Auth\Models\Builders\UserBuilder
     */
    protected function modifyQuery(Builder $query): Builder
    {
        return $query->with(['role']);
    }
}
