<?php

declare(strict_types=1);

namespace Smorken\Auth\Repositories;

use Smorken\Auth\Contracts\Models\User;
use Smorken\Domain\Repositories\EloquentRetrieveRepository;

/**
 * @extends EloquentRetrieveRepository<\Smorken\Auth\Models\Eloquent\User>
 */
class FindUserRepository extends EloquentRetrieveRepository implements \Smorken\Auth\Contracts\Repositories\FindUserRepository
{
    public function __construct(User $model)
    {
        parent::__construct($model);
    }
}
