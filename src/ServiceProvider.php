<?php

declare(strict_types=1);

namespace Smorken\Auth;

use Illuminate\Contracts\Foundation\Application;
use Smorken\Roles\Contracts\Role;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot(): void
    {
        $this->loadConfig();
        $this->loadMigrations();
        $this->handlePublishing();
    }

    public function register(): void
    {
        $this->registerModels();
        $this->registerRepositories();
    }

    protected function handlePublishing(): void
    {
        $this->publishes([
            __DIR__.'/../config/config.php' => config_path('sm-auth'),
        ], 'sm-auth-config');
        if ($this->app['config']->get('sm-auth.load_migrations', true)) {
            $this->publishes([
                __DIR__.'/../database/migrations/' => database_path('migrations'),
            ], 'sm-auth-migrations');
        }
    }

    protected function isRolePackageBound(Application $app): bool
    {
        return $app->bound(Role::class);
    }

    protected function loadConfig(): void
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/config.php',
            'sm-auth'
        );
    }

    protected function loadMigrations(): void
    {
        if ($this->app['config']->get('sm-auth.load_migrations', true)) {
            $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        }
    }

    protected function registerModels(): void
    {
        $this->booted(function (Application $app) {
            foreach ($app['config']->get('sm-auth.models', []) as $contract => $impl) {
                $app->scoped($contract, static fn (Application $app) => $app[$impl]);
            }
        });
    }

    protected function registerRepositories(): void
    {
        $this->booted(function (Application $app) {
            foreach ($app['config']->get('sm-auth.repositories', []) as $contract => $impl) {
                $app->scoped($contract, static fn (Application $app) => $app[$impl]);
            }
        });
    }
}
