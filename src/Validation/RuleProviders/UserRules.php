<?php

declare(strict_types=1);

namespace Smorken\Auth\Validation\RuleProviders;

class UserRules
{
    public static function rules(array $overrides = []): array
    {
        return [
            'id' => 'required|integer',
            'username' => 'required|string',
            'email' => 'required|email',
        ];
    }
}
