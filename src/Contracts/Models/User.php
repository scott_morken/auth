<?php

declare(strict_types=1);

namespace Smorken\Auth\Contracts\Models;

use Illuminate\Contracts\Auth\Authenticatable;
use Smorken\Model\Contracts\Model;

/**
 * @property string|int $id
 * @property string $username
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * Virtual Attributes
 * @property string $fullName
 * @property string $shortName
 * @property string $activeRole
 */
interface User extends Authenticatable, Model
{
    public function isValid(): bool;

    public function name(): string;
}
