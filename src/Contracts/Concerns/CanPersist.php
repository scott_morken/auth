<?php

declare(strict_types=1);

namespace Smorken\Auth\Contracts\Concerns;

interface CanPersist {}
