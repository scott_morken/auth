<?php

declare(strict_types=1);

namespace Smorken\Auth\Contracts\Repositories;

use Smorken\Domain\Repositories\Contracts\FilteredRepository;

interface FilteredUsersRepository extends FilteredRepository {}
