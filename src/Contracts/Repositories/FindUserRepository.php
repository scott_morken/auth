<?php

declare(strict_types=1);

namespace Smorken\Auth\Contracts\Repositories;

use Smorken\Domain\Repositories\Contracts\RetrieveRepository;

interface FindUserRepository extends RetrieveRepository {}
