<?php

declare(strict_types=1);

return [
    'load_migrations' => true,
    'models' => [
        \Smorken\Auth\Contracts\Models\User::class => \Smorken\Auth\Models\Eloquent\User::class,
    ],
    'repositories' => [
        \Smorken\Auth\Admin\Contracts\Repositories\FilteredUsersRepository::class => \Smorken\Auth\Admin\Repositories\FilteredUsersRepository::class,
        \Smorken\Auth\Admin\Contracts\Repositories\FindUserRepository::class => \Smorken\Auth\Admin\Repositories\FindUserRepository::class,
        \Smorken\Auth\Admin\External\Contracts\Repositories\FilteredUsersRepository::class => \Smorken\Auth\Admin\External\Repositories\MsGraph\FilteredUsersRepository::class,
        \Smorken\Auth\Admin\External\Contracts\Repositories\FindUserRepository::class => \Smorken\Auth\Admin\External\Repositories\MsGraph\FindUserRepository::class,
    ],
];
