<?php

declare(strict_types=1);

namespace Tests\Smorken\Auth\Phpstan\Models;

use Smorken\Auth\Models\Builders\UserBuilder;
use Smorken\Auth\Models\Eloquent\User;

class UserStan extends User {}

$m = new UserStan;
assert($m->newQuery()->usernameIs('foo') instanceof UserBuilder);
$m->username = 'foo';
assert($m->username === 'foo');
