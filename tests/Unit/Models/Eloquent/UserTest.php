<?php

declare(strict_types=1);

namespace Tests\Smorken\Auth\Unit\Models\Eloquent;

use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\Auth\Models\Eloquent\User;
use Tests\Smorken\Auth\Concerns\WithMockConnection;

class UserTest extends TestCase
{
    use WithMockConnection;

    #[Test]
    public function it_creates_a_query_for_role_including_code(): void
    {
        $sut = (new User)->newQuery()->roleIncludesCode('manage');
        $this->assertEquals('select * from `users` where exists (select * from `role_user` inner join `roles` on `roles`.`id` = `role_user`.`role_id` where `roles`.`level` >= (select `level` from `roles` where `code` = ?) and `users`.`id` = `role_user`.`user_id`)',
            $sut->toSql());
        $this->assertEquals(['manage'], $sut->getBindings());
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->initConnectionResolver();
    }
}
