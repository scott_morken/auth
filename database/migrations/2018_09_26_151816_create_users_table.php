<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::drop('users');
    }

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->string('id', 72);
            $table->string('username')
                ->unique();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')
                ->unique();
            $table->timestamp('email_verified_at')
                ->nullable();
            $table->string('password')->default('*AUTH*');
            $table->rememberToken();
            $table->mediumText('data')
                ->nullable();
            $table->timestamps();

            $table->primary('id');
            $table->unique('username', 'users_username_ndx');
            $table->index('first_name', 'users_fn_ndx');
            $table->index('last_name', 'users_ln_ndx');
            $table->index('email', 'users_email_ndx');
        });
    }
};
