<?php

namespace Database\Factories\Smorken\Auth\Models\Eloquent;

use Illuminate\Database\Eloquent\Factories\Factory;
use Smorken\Auth\Models\Eloquent\User;
use Smorken\Support\Str;

class UserFactory extends Factory
{
    protected $model = User::class;

    public function definition(): array
    {
        $id = rand(30000000, 39999999);

        return [
            'id' => $id,
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'email' => $this->faker->safeEmail,
            'username' => Str::random(5).$this->faker->randomNumber(5),
            'password' => '*SOCIAL*',
        ];
    }
}
